const config = {
  name: 'JS数据集',
  datasetType: 'js',
  className: 'com.gccloud.dataset.entity.config.JsDataSetConfig',
  componentName: 'JsDataSet',
  showOperate: true
}

export default {
  config
}
